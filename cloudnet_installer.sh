#!/bin/bash

INSTALLERVERSION="1.0.1"

function greenMessage {
  echo -e "\\033[32;1m${*}\033[0m"
}

function magentaMessage {
  echo -e "\\033[35;1m${*}\033[0m"
}

function cyanMessage {
  echo -e "\\033[36;1m${*}\033[0m"
}

function redMessage {
  echo -e "\\033[31;1m${*}\033[0m"
}

function yellowMessage {
  echo -e "\\033[33;1m${*}\033[0m"
}

function errorQuit {
  errorExit 'Exit now!'
}

function errorExit {
  redMessage "${@}"
  exit 1
}

function errorContinue {
  redMessage "Invalid option."
  return
}

function checktext {

	if [[ $(grep -m 1 "$1" $CNSDIR/.setuplog 2>&1 | head -c1 | wc -c) -ne 0 ]] ; then
	  return 0
	else
	  return 1
	fi

}

function download_proxy {

  cd /tmp >/dev/null 2>&1 #
  cyanMessage "Step 1 / 5 complete!"
  rm -rf /tmp/CloudNet.zip >/dev/null 2>&1 #
  wget -q -O /tmp/CloudNet.zip http://dytanic.de/cloudnetproject/CloudNet.zip >/dev/null 2>&1 #
  cyanMessage "Step 2 / 5 complete!"
  rm -rf /tmp/.cloudnetinstaller >/dev/null 2>&1 #
  mkdir -p /tmp/.cloudnetinstaller >/dev/null 2>&1 #
  cyanMessage "Step 3 / 5 complete!"
  unzip CloudNet.zip -d ./.cloudnetinstaller >/dev/null 2>&1 #
  cyanMessage "Step 4 / 5 complete!"
  cp -R /tmp/.cloudnetinstaller/CloudNet-Proxy/CloudNet-Proxy.jar "$CNPDIR" >/dev/null 2>&1 #
  cyanMessage "Step 5 / 5 complete! Running cleanup..."
  rm -rf /tmp/.cloudnetinstaller >/dev/null 2>&1 #
  rm -rf /tmp/CloudNet.zip >/dev/null 2>&1 #

}

function download_server {

  cd /tmp >/dev/null 2>&1 #
  cyanMessage "Step 1 / 5 complete!"
  rm -rf CloudNet.zip >/dev/null 2>&1 #
  wget -q -O CloudNet.zip http://dytanic.de/cloudnetproject/CloudNet.zip >/dev/null 2>&1 #
  cyanMessage "Step 2 / 5 complete!"
  rm -rf /tmp/.cloudnetinstaller >/dev/null 2>&1 #
  mkdir -p /tmp/.cloudnetinstaller >/dev/null 2>&1 #
  cyanMessage "Step 3 / 5 complete!"
  unzip CloudNet.zip -d ./.cloudnetinstaller >/dev/null 2>&1 #
  cyanMessage "Step 4 / 5 complete!"
  cp -R /tmp/.cloudnetinstaller/CloudNet-Server/CloudNet-Server.jar "$CNSDIR" >/dev/null 2>&1 #
  cyanMessage "Step 5 / 5 complete! Running cleanup..."
  rm -rf /tmp/.cloudnetinstaller >/dev/null 2>&1 #
  rm -rf /tmp/CloudNet.zip >/dev/null 2>&1 #

}

function download_proxy_survey {

  yellowMessage "Where should the CloudNet proxy be saved?"
	CNPDIR=""
	while [[ ! -d $CNPDIR ]]; do
	  read -rp "Directory [/home/cloudnet/proxy]: " CNPDIR
		if [ "$CNPDIR" == "" ] ; then
		  CNPDIR=/home/cloudnet/proxy
		fi
		if [ ! -d $CNPDIR ] ; then
		  mkdir -p "$CNPDIR" >/dev/null 2>&1 #
		fi
	done

	if [ -f "$CNPDIR/CloudNet-Proxy.jar" ] ; then

	  redMessage "CloudNet proxy jar file found. Should the installer remove it?"

		OPT1S=("Yes" "No" "Quit")
		select OPT1 in "${OPT1S[@]}" ; do
			case "$REPLY" in
			  1|2 ) break;;
			  3 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT1 == "Yes" ]]; then
		  redMessage "Removing old CloudNet proxy jar file..."
		  rm -rf $CNPDIR/CloudNet-Proxy.jar >/dev/null 2>&1 #
			if [ ! -f "$CNPDIR/CloudNet-Proxy.jar" ] ; then
			  greenMessage "Old CloudNet proxy jar file removed!"
			else
			  redMessage "Couldn't remove CloudNet proxy jar file. Please remove it manually."
			fi
		elif [[ $OPT1 == "No" ]] ; then
		  errorExit
		fi
	fi

  echo ""
  cyanMessage "Downloading CloudNet proxy jar file... This may take up to 2 minutes..."

  download_proxy

}

function download_server_survey {

  yellowMessage "Where should the CloudNet server be saved?"

	CNSDIR=""
	while [[ ! -d $CNSDIR ]]; do
	  read -rp "Directory [/home/cloudnet/server]: " CNSDIR
		if [ "$CNSDIR" == "" ] ; then
		  CNSDIR=/home/cloudnet/server
		fi
		if [ ! -d $CNSDIR ] ; then
		  mkdir -p "$CNSDIR" >/dev/null 2>&1 #
		fi
	done

	if [ -f "$CNSDIR/CloudNet-Server.jar" ] ; then

	  redMessage "CloudNet server jar file found. Should the installer remove it?"

		OPT1S=("Yes" "No" "Quit")
		select OPT1 in "${OPT1S[@]}" ; do
			case "$REPLY" in
			  1|2 ) break;;
			  3 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT1 == "Yes" ]]; then
		  redMessage "Removing old CloudNet server jar file..."
		  rm -rf $CNSDIR/CloudNet-Server.jar >/dev/null 2>&1 #
			if [ ! -f "$CNSDIR/CloudNet-Server.jar" ] ; then
			  greenMessage "Old CloudNet server jar file removed!"
			else
			  redMessage "Couldn't remove CloudNet server jar file. Please remove it manually."
			fi
		elif [[ $OPT1 == "No" ]] ; then
		  errorExit
		fi
	fi

  echo ""
  cyanMessage "Downloading CloudNet server jar file... This may take up to 2 minutes..."

  download_server

}

function start_proxy {

	for session in $(screen -ls | grep -o '[0-9]*\.cloudnetproxy'); do screen -S "${session}" -X quit; done
	for session in $(screen -ls | grep -o '[0-9]*\.cloudnetproxysetup'); do screen -S "${session}" -X quit; done

  rm -rf $CNPDIR/.setuplog >/dev/null 2>&1 #
  touch $CNPDIR/.setuplog >/dev/null 2>&1 #
  echo -e "# Logfile\nlogfile $CNPDIR/.setuplog\n" > $CNPDIR/.setupscreenrc
  cd "$CNPDIR" && screen -c $CNPDIR/.setupscreenrc -LdmS cloudnetproxysetup bash -c "java -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:MaxPermSize=256M -Xmx64m -jar CloudNet-Proxy.jar"
  
	for (( ; ; )) ; do
	  sleep 0.1
		if [ -s $CNPDIR/.setuplog ] ; then
		  break
		fi
	done

	if checktext "Your IP address where located is 127.0.0.1 please write your server ip" ; then
	  screen -S cloudnetproxysetup -p 0 -X stuff "127.0.0.1^M" >/dev/null 2>&1 #
	fi
	sleep 1

	if [ -f "$CNPDIR/config.yml" ] ; then
	  SERVICEKEY=$(cat $CNPDIR/config.yml | grep "servicekey" | cut -d ' ' -f 2)
	else
	  SERVICEKEY=$(sed -n 's|[^<]*ServiceKey: \"\([^<]*\)\"[^<]*|\1\n|gp' $CNPDIR/.setuplog | head -1)
	fi
  
	for i in {1..10} ; do
	  sleep 1
		if checktext "CNS" ; then
		  greenMessage "Proxy started successfully!"
		  break
		else
		  redMessage "Error while starting CloudNet proxy! Log:"
		  cat $CNPDIR/proxy.log
		  echo ""
		  yellowMessage "Should the installer go on?"

			OPT1S=("Yes" "No" "Quit")
			select OPT1 in "${OPT1S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT1 == "Yes" ]]; then
			  return 0
			elif [[ $OPT1 == "No" ]] ; then
			  exit 1
			fi
		fi
	done

  for session in $(screen -ls | grep -o '[0-9]*\.cloudnetproxysetup'); do screen -S "${session}" -X quit; done	
  rm -rf $CNPDIR/.setuplog >/dev/null 2>&1 #
  rm -rf $CNPDIR/.setupscreenrc >/dev/null 2>&1 #

}

function start_server {

	for session in $(screen -ls | grep -o '[0-9]*\.cloudnetserver'); do screen -S "${session}" -X quit; done
	for session in $(screen -ls | grep -o '[0-9]*\.cloudnetserversetup'); do screen -S "${session}" -X quit; done
  sleep 3
  rm -rf $CNSDIR/.setuplog >/dev/null 2>&1 #
  touch $CNSDIR/.setuplog >/dev/null 2>&1 #
  echo -e "# Logfile\nlogfile $CNSDIR/.setuplog\n# Lastline Info\nautodetach on\nstartup_message off\nhardstatus alwayslastline\nshelltitle 'bash'\nhardstatus string '%{gk}Info: Press CTRL(STRG) + C when you are finished!'" > $CNSDIR/.setupscreenrc
  cd $CNSDIR && screen -c "$CNSDIR/.setupscreenrc" -LdmS cloudnetserversetup bash -c "java -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:MaxPermSize=256M -Xmx32m -jar CloudNet-Server.jar"
  
	for (( ; ; )) ; do
	  sleep 0.1
		if [ -s $CNSDIR/.setuplog ] ; then
		  break
		fi
	done

	  sleep 1
		if checktext "Thanks" ; then
		  redMessage "2Error while starting CloudNet server! Log:"
		  cat $CNSDIR/.setuplog
		  yellowMessage "Retrying in 5 seconds..."
		  sleep 5
		  start_server
		elif checktext "language" || checktext "key" || checktext "BungeeCord" || checktext "minecraft" || checktext "name" || checktext "How" || checktext "joinpower" || checktext "may" || checktext "online" || checktext "permanently" || checktext "groupmode" || checktext "servertype" ; then
		  redMessage "IMPORTANT:"
		  cyanMessage "CloudNet server instance started successfully but setup is not complete! Entering screen session in 10 seconds..."
		  cyanMessage "Please remember: you can exit the screen session by pressing \"CTRL(STRG) + C\" when you finished the setup!"
		  sleep 10
		  screen -r cloudnetserversetup
		elif checktext "starting..." || checktext "command" ; then
		  greenMessage "Server instance started successfully!"
		else
		  redMessage "1Error while starting CloudNet server! Log:"
		  cat "$CNSDIR/.setuplog"

		  yellowMessage "Should the installer go on?"

			OPT1S=("Retry" "Quit")
			select OPT1 in "${OPT1S[@]}" ; do
				case "$REPLY" in
				  1 ) break;;
				  2 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT1 == "Retry" ]]; then
			  start_server
			elif [[ $OPT1 == "Quit" ]] ; then
			  exit 1
			fi
		fi

  for session in $(screen -ls | grep -o '[0-9]*\.cloudnetproxysetup'); do screen -S "${session}" -X quit; done
  rm -rf $CNSDIR/.setuplog >/dev/null 2>&1 #
  rm -rf $CNSDIR/.setupscreenrc >/dev/null 2>&1 #
}

function install_proxy {

  redMessage "IMPORTANT: Please do not rename the jarfile. The installer will not work if the name isn't \"CloudNet-Proxy.jar\"!!"
  echo ""
  sleep 5

# Install

  yellowMessage "Where should the CloudNet proxy instance be installed?"

	CNPDIR=""
	while [[ ! -d $CNPDIR ]]; do
	  read -rp "Directory [/home/cloudnet/proxy]: " CNPDIR
		if [ "$CNPDIR" == "" ] ; then
		  CNPDIR=/home/cloudnet/proxy
		fi
		if [ ! -d $CNPDIR ] ; then
		  mkdir -p "$CNPDIR"
		fi
	done

	if [ -f "$CNPDIR/CloudNet-Proxy.jar" ] ; then

	  redMessage "CloudNet proxy instance found! Should the installer remove the current version? (No Userdata! Just the jarfile.)"

		OPT1S=("Yes" "No" "Quit")
		select OPT1 in "${OPT1S[@]}" ; do
			case "$REPLY" in
			  1|2 ) break;;
			  3 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT1 == "Yes" ]]; then
		  redMessage "Removing old CloudNet proxy instance..."
		  rm -rf $CNPDIR/CloudNet-Proxy.jar
			if [ ! -f "$CNPDIR/CloudNet-Proxy.jar" ] ; then
			  greenMessage "Old CloudNet proxy instance removed!"
			else
			  redMessage "Cloudn't remove old CloudNet proxy instance. Please remove it manually."
			fi
		elif [[ $OPT1 == "No" ]] ; then
		  echo "" >/dev/null 2>&1
		fi
	fi
  echo ""
  cyanMessage "installing CloudNet proxy instance... This may take up to 5 minutes..."

  download_proxy
  echo -e "# Logfile\nlogfile $CNPDIR/proxy.log\n# Lastline Info\nautodetach on\nstartup_message off\nhardstatus alwayslastline\nshelltitle 'bash'\nhardstatus string '%{gk}Info: Press CTRL(STRG) + A + D to exit the screen session!'" > $CNPDIR/.screenrc
  
  echo ""
  greenMessage "Cleanup complete! CloudNet proxy instance installed successfully!"

# Autostart activation

  echo ""
  yellowMessage "Should the CloudNet proxy instance start at system boot?"

	OPT3S=("Yes" "No" "Quit")
	select OPT3 in "${OPT3S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT3 == "Yes" ]] ; then
	  rm -rf /etc/init.d/cloudnetproxy >/dev/null 2>&1
	  echo "#! /bin/bash

### BEGIN INIT INFO
# Provides:             CNP
# Required-Start:       \$local_fs \$network
# Required-Stop:        \$local_fs \$network
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Description:          CloudNet cloudsystem proxy instance
### END INIT INFO

CNPDIR=$CNPDIR

function screen_exists {

if screen -list | grep -q \"cloudnetproxy\"; then
  return 0
else
  return 1
fi

}

function start {

  cd \$CNPDIR && screen -c \$CNPDIR/.screenrc -LdmS cloudnetproxy bash -c \"java -XX:MaxGCPauseMillis=50 -Xmx128m -jar CloudNet-Proxy.jar\"

}

function stop {

  for session in \$(screen -ls | grep -o '[0-9]*\.cloudnetproxy'); do screen -S \"\${session}\" -X quit; done

}

function print_help {

  echo \"
Usage: {start | stop | restart | stauts  | help}

start   - start the CloudNet server instance
stop    - stop the CloudNet server instance
restart - restart the CloudNet server instance
status  - check if the CloudNet server instance is running
help    - show the help page
\" >&2
}

case \"\$1\" in

start)

if ! screen_exists ; then
  start
  echo \"CloudNet proxy instance started\"
else
  echo \"CloudNet proxy instance is already running\"
fi

;;

stop)

if screen_exists ; then
  stop
  echo \"CloudNet proxy instance stopped\"
else
  echo \"CloudNet proxy instance is already stopped\"
fi

;;

restart)

if screen_exists ; then
  stop
  echo \"CloudNet proxy instance stopped\"
  start
  echo \"CloudNet proxy instance started\"
else
  start
  echo \"CloudNet proxy instance started\"
fi

;;

status)

if screen_exists ; then
  echo \"CloudNet proxy instance is running\"
else
  echo \"CloudNet proxy instance is not running\"
fi

;;

help)

  print_help

;;

*)

  print_help

  exit 1

;;

esac" > /etc/init.d/cloudnetproxy

	  chmod 755 /etc/init.d/cloudnetproxy >/dev/null 2>&1 #
	  
	  if [ "$OS" == "debian" ] || [ "$OS" == "ubuntu" ] ; then
	    update-rc.d cloudnetproxy defaults >/dev/null 2>&1 #
	  elif [ "$OS" == "centos" ] ; then
		chkconfig cloudnetproxy on >/dev/null 2>&1 #
	  fi
	  
	  
	  echo ""
	  greenMessage "CloudNet proxy instance was added to system boot successfully!"
	elif [[ $OPT3 == "No" ]] ; then
	  echo ""
	  redMessage "Proxy instance will not start at system boot"
	fi

	if [ ! -f "$CNPDIR/config.yml" ] ; then
	  cyanMessage "starting the CloudNet proxy for the first time..."
	  start_proxy
	  echo ""
	else
	  cyanMessage "starting the CloudNet proxy..."
	  start_proxy
	  echo ""
	fi
	
  redMessage "-------------------------------------------------------------"
  cyanMessage "Your CloudNet proxy instance servicekey: $SERVICEKEY"
  redMessage "-------------------------------------------------------------"

}

function install_server {

  redMessage "IMPORTANT: Please do not rename the jarfile. The installer will not work if the name isn't \"CloudNet-Server.jar\"!!"
  sleep 5

# Install

  echo ""
  yellowMessage "Where should the CloudNet server instance be installed?"

	CNSDIR=""
	while [[ ! -d $CNSDIR ]]; do
	  read -rp "Directory [/home/cloudnet/server]: " CNSDIR
		if [ "$CNSDIR" == "" ] ; then
		  
		  CNSDIR=/home/cloudnet/server
		  echo "$CNSDIR 2"
		fi
		if [[ ! -d $CNSDIR ]] ; then
		  mkdir -p "$CNSDIR" >/dev/null 2>&1
		fi
	done

	if [ -f "$CNSDIR/CloudNet-Server.jar" ] ; then

	  redMessage "CloudNet server instance found! Should the installer remove the current version? (No Userdata! Just the jarfile.)"

		OPT1S=("Yes" "No" "Quit")
		select OPT1 in "${OPT1S[@]}" ; do
			case "$REPLY" in
			  1|2 ) break;;
			  3 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT1 == "Yes" ]]; then

		  redMessage "Removing old CloudNet server instance..."
		  rm -rf $CNSDIR/CloudNet-Server.jar >/dev/null 2>&1 #
			if [ ! -f "$CNSDIR/CloudNet-Server.jar" ] ; then
			  greenMessage "Old CloudNet server instance removed!"
			else
			  redMessage "Cloudn't remove old CloudNet server instance. Please remove it manually."
			fi
		elif [[ $OPT1 == "No" ]] ; then
		  echo "" >/dev/null 2>&1
		fi
	fi

  echo ""
  cyanMessage "installing CloudNet server instance... This may take up to 5 minutes..."

  download_server
  echo "logfile $CNSDIR/server.log" > $CNSDIR/.screenrc

  echo ""
  greenMessage "Cleanup complete! CloudNet server instance installed successfully!"

# Autostart activation

  echo ""
  yellowMessage "Should the CloudNet server instance start at system boot?"

	OPT3S=("Yes" "No" "Quit")
	select OPT3 in "${OPT3S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT3 == "Yes" ]] ; then
	  echo "" >/dev/null 2>&1
	  rm -rf /etc/init.d/cloudnetserver >/dev/null 2>&1
	  echo "#! /bin/bash

### BEGIN INIT INFO
# Provides:             CNS
# Required-Start:       \$local_fs \$network
# Required-Stop:        \$local_fs \$network
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Description:          CloudNet cloudsystem server instance
### END INIT INFO

CNSDIR=$CNSDIR

function screen_exists {

if screen -list | grep -q \"cloudnetserver\"; then
  return 0
else
  return 1
fi

}

function start {

  cd \$CNSDIR && screen -c \$CNSDIR/.screenrc -LdmS cloudnetserver bash -c \"java -XX:MaxGCPauseMillis=50 -Xmx128m -jar CloudNet-Server.jar\"

}

function stop {

  for session in \$(screen -ls | grep -o '[0-9]*\.cloudnetserver'); do screen -S \"\${session}\" -X quit; done

}

function print_help {

  echo \"
Usage: {start | stop | restart | stauts  | help}

start   - start the CloudNet server instance
stop    - stop the CloudNet server instance
restart - restart the CloudNet server instance
status  - check if the CloudNet server instance is running
help    - show the help page
\" >&2
}

case \"\$1\" in

start)

if ! screen_exists ; then
  start
  echo \"CloudNet server instance started\"
else
  echo \"CloudNet server instance is already running\"
fi

;;

stop)

if screen_exists ; then
  stop
  echo \"CloudNet server instance stopped\"
else
  echo \"CloudNet server instance is already stopped\"
fi

;;

restart)

if screen_exists ; then
stop
  echo \"CloudNet server instance stopped\"
  start
  echo \"CloudNet server instance started\"
else
  start
  echo \"CloudNet server instance started\"
fi

;;

status)

if screen_exists ; then
  echo \"CloudNet server instance is running\"
else
  echo \"CloudNet server instance is not running\"
fi

;;

help)

  print_help

;;

*)

  print_help
  exit 1

;;

esac" > /etc/init.d/cloudnetserver

	  chmod 755 /etc/init.d/cloudnetserver  >/dev/null 2>&1 #

	  if [ "$OS" == "debian" ] || [ "$OS" == "ubuntu" ] ; then
	    update-rc.d cloudnetserver defaults >/dev/null 2>&1 #
	  elif [ "$OS" == "centos" ] ; then
		chkconfig cloudnetserver on >/dev/null 2>&1 #
	  fi

	  echo ""
	  greenMessage "CloudNet server instance was added to system boot successfully!"

	elif [[ $OPT3 == "No" ]] ; then

	  echo ""
	  redMessage "Server instance will not start at system boot"

	fi

	if [ ! -f "$CNSDIR/config.yml" ] ; then
	  cyanMessage "starting the CloudNet server for the first time..."
	  start_server
	  echo ""
	else
	  cyanMessage "starting the CloudNet server..."
	  start_server
	  echo ""
	fi

}

function remove_proxy_jarfile {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet proxy jarfile?"

	OPT7S=("No" "Yes" "Quit")
	select OPT7 in "${OPT7S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT7 == "No" ]]; then

	  remove_proxy_survey

	elif [[ $OPT7 == "Yes" ]]; then

	  rm -rf $PROXYPATH/CloudNet-Proxy.jar >/dev/null 2>&1 #

		if [ ! -a "$PROXYPATH/CloudNet-Proxy.jar" ] ; then

		  greenMessage "CloudNet proxy jarfile removed!"

		else

		  redMessage "Cloudn't remove CloudNet proxy jarfile. Please remove it manually."

		fi
	fi
}

function remove_proxy_userdata {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet proxy userdata? This includes EVERY file and EVERY folder except the jarfile."

	OPT8S=("No" "Yes" "Quit")
	select OPT8 in "${OPT8S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT8 == "No" ]]; then

	  remove_proxy_survey

	elif [[ $OPT8 == "Yes" ]]; then

	  cd $PROXYPATH && ls | grep -v CloudNet-Proxy.jar | xargs rm -rf >/dev/null 2>&1 #

	  FILESANDFOLDERS=`ls "$PROXYPATH" 2>/dev/null | wc -l`

		if [ "$FILESANDFOLDERS" == 1 ] || [ "$FILESANDFOLDERS" == 0 ] ; then

		  greenMessage "CloudNet proxy userdata removed!"

		else

		  redMessage "Cloudn't remove CloudNet proxy userdata. Please remove it manually."

		fi

	fi
}

function remove_proxy {

  echo ""
  yellowMessage "What should be removed?"

	OPT6S=("Jarfile" "Userdata" "Both" "Quit")
	select OPT6 in "${OPT6S[@]}" ; do
		case "$REPLY" in
		  1|2|3 ) break;;
		  4 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT6 == "Jarfile" ]]; then

	  remove_proxy_jarfile

	elif [[ $OPT6 == "Userdata" ]]; then

	  remove_proxy_userdata

	elif [[ $OPT6 == "Both" ]]; then

	  remove_proxy_jarfile
	  remove_proxy_userdata

	fi

	if [ "$OS" == "debian" ] || [ "$OS" == "ubuntu" ] ; then
	  update-rc.d cloudnetproxy disable >/dev/null 2>&1 #
	elif [ "$OS" == "centos" ] ; then
	  chkconfig cloudnetproxy off >/dev/null 2>&1 #
	fi
  rm -rf /etc/init.d/cloudnetproxy >/dev/null 2>&1 #
}

function remove_proxy_survey {

  echo ""
  yellowMessage "please enter the CloudNet proxy instance path."

	while [[ -d $PROXYPATH ]]; do
	  read -rp "Path [/home/cloudnet/proxy]:" PROXYPATH
		if [ "$PROXYPATH" == "" ] ; then
		  PROXYPATH=/home/cloudnet/server
		fi
		if [ ! -d "$PROXYPATH" ] ; then
		  redMessage "Path doesn't exist. Pease enter a valid path."
		fi
		
		if [ -d "$PROXYPATH/database" ] || [ -f "$PROXYPATH/CloudNet-Proxy.jar" ] ; then

		  remove_proxy

		else

		  redMessage "CloudNet proxy instance installation not found."

		  echo ""
		  yellowMessage "Continue removing?"

			OPT5S=("Yes" "No" "Quit")
			select OPT5 in "${OPT5S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT5 == "Yes" ]]; then

			  remove_proxy

			elif [[ $OPT5 == "No" ]]; then

			  errorQuit

			fi
		fi
	done

}

function remove_server_jarfile {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet server jarfile?"

	OPT7S=("No" "Yes" "Quit")
	select OPT7 in "${OPT7S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT7 == "No" ]]; then

	  remove_server_survey

	elif [[ $OPT7 == "Yes" ]]; then

	  rm -rf $SERVERPATH/CloudNet-Server.jar >/dev/null 2>&1 #

		if [ ! -a "$SERVERPATH/CloudNet-Server.jar" ] ; then

		  greenMessage "CloudNet server jarfile removed!"

		else

		  redMessage "Cloudn't remove CloudNet server jarfile. Please remove it manually."

		fi

	fi

}

function remove_server_userdata {

  echo ""
  yellowMessage "Do you really want to remove the CloudNet server userdata? This includes EVERY file and EVERY folder except the jarfile."

	OPT8S=("No" "Yes" "Quit")
	select OPT8 in "${OPT8S[@]}" ; do
		case "$REPLY" in
		  1|2 ) break;;
		  3 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT8 == "No" ]]; then

	  remove_server_survey

	elif [[ $OPT8 == "Yes" ]]; then

	  cd $SERVERPATH && ls | grep -v CloudNet-Server.jar | xargs rm -rf >/dev/null 2>&1 #

	  FILESANDFOLDERS=`ls "$SERVERPATH" 2>/dev/null | wc -l`

		if [ "$FILESANDFOLDERS" == 1 ] || [ "$FILESANDFOLDERS" == 0 ] ; then

		  greenMessage "CloudNet server userdata removed!"

		else

		  redMessage "Cloudn't remove CloudNet server userdata. Please remove it manually."

		fi

	fi

}

function remove_server {

  echo ""
  yellowMessage "What should be removed?"

	OPT6S=("Jarfile" "Userdata" "Both" "Quit")
	select OPT6 in "${OPT6S[@]}" ; do
		case "$REPLY" in
		  1|2|3 ) break;;
		  4 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT6 == "Jarfile" ]]; then

	  remove_server_jarfile

	elif [[ $OPT6 == "Userdata" ]]; then

	  remove_server_userdata

	elif [[ $OPT6 == "Both" ]]; then

	  remove_server_jarfile
	  remove_server_userdata

	fi

	if [ "$OS" == "debian" ] || [ "$OS" == "ubuntu" ] ; then
	  update-rc.d cloudnetserver disable >/dev/null 2>&1 #
	elif [ "$OS" == "centos" ] ; then
	  chkconfig cloudnetserver off >/dev/null 2>&1 #
	fi
  rm -rf /etc/init.d/cloudnetserver >/dev/null 2>&1 #
}

function remove_server_survey {

  echo ""
  yellowMessage "please enter the CloudNet server instance path."

	while [[ ! -d $SERVERPATH ]]; do
	  read -rp "Path [/home/cloudnet/server]:" SERVERPATH
		if [ "$SERVERPATH" == "" ] ; then
		  SERVERPATH=/home/cloudnet/server
		fi
		if [ ! -d "$SERVERPATH" ] ; then
		  redMessage "Path doesn't exist. Pease enter a valid path."
		fi
		
		if [ -d "$SERVERPATH/database" ] || [ -f "$SERVERPATH/CloudNet-Server.jar" ] ; then

		  remove_server

		else

		  redMessage "CloudNet server instance installation not found."

		  echo ""
		  yellowMessage "Continue removing?"

			OPT5S=("Yes" "No" "Quit")
			select OPT5 in "${OPT5S[@]}" ; do
				case "$REPLY" in
				  1|2 ) break;;
				  3 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

			if [[ $OPT5 == "Yes" ]]; then

			  remove_server

			elif [[ $OPT5 == "No" ]]; then

			  errorQuit

			fi
		fi
	done
}

function initial {

  greenMessage "This is the installer for CloudNet 1.0.5. USE AT YOUR OWN RISK"!
  sleep 2
  cyanMessage "You can choose between installing and removeing CloudNet."
  sleep 2
  magentaMessage "Installer version: $INSTALLERVERSION"
  sleep 1
  redMessage "Script written by Arnim S. @ https://www.eu-hosting.ch"
  sleep 1

  echo ""
  yellowMessage "What should the script do?"

	OPT1S=("Install" "Download" "Remove" "Quit")
	select OPT1 in "${OPT1S[@]}" ; do
		case "$REPLY" in
		  1|2|3 ) break;;
		  4 ) errorQuit;;
		  *) errorContinue;;
		esac
	done

	if [[ $OPT1 == "Install" ]]; then

	  echo ""
	  yellowMessage "What should be installed?"

		OPT2S=("Proxy" "Server" "Both" "Quit")
		select OPT2 in "${OPT2S[@]}" ; do
			case "$REPLY" in
			  1|2|3 ) break;;
			  4 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT2 == "Proxy" ]] ; then

		  echo ""
		  cyanMessage "> CLOUDNET PROXY INSTANCE INSTALLATION <"
		  echo ""

		  install_proxy

		elif [[ $OPT2 == "Server" ]] ; then

		  echo ""
		  cyanMessage "> CLOUDNET SERVER INSTANCE INSTALLATION <"
		  echo ""

		  install_server

		elif [[ $OPT2 == "Both" ]] ; then

		  echo ""
		  cyanMessage "> CLOUDNET PROXY INSTANCE INSTALLATION <"
		  echo ""

		  install_proxy

		  echo ""
		  cyanMessage "> CLOUDNET SERVER INSTANCE INSTALLATION <"
		  echo ""

		  install_server

		fi

	elif [[ $OPT1 == "Download" ]]; then

	  echo ""
	  yellowMessage "What should be downloaded?"

		OPT5S=("Proxy" "Server" "Both" "Quit")
		select OPT5 in "${OPT5S[@]}" ; do
			case "$REPLY" in
			  1|2|3 ) break;;
			  4 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT5 == "Proxy" ]]; then

		  download_proxy_survey
		  echo ""
		  greenMessage "Cleanup complete! CloudNet proxy jar file downloaded successfully!"

		elif [[ $OPT5 == "Server" ]]; then

		  download_server_survey
		  echo ""
		  greenMessage "Cleanup complete! CloudNet server jar file downloaded successfully!"

		elif [[ $OPT5 == "Both" ]]; then

		  yellowMessage "Where should the ZIP file be saved?"

			while [[ ! -d $CNDLDIR ]]; do
			  read -p "Directory [/home/cloudnet]: " CNDLDIR
				if [ "$CNDLDIR" == "" ] ; then
				  CNDLDIR=/home/cloudnet
				fi
				if [ ! -d $CNDLDIR ] ; then
				  mkdir -p "$CNDLDIR" >/dev/null 2>&1 #
				fi
			done

			if [ "$CNDLDIR" == "" ] ; then
			  CNDLDIR=/home/cloudnet
			fi

			if [ -d "$CNDLDIR" ]; then

			  echo ""
			  redMessage "Not creating directory because it already exists."

				if [ -f "$CNDLDIR/CloudNet.zip" ] ; then

				  redMessage "CloudNet ZIP file found. Should the installer remove it?"

					OPT1S=("Yes" "No" "Quit")
					select OPT1 in "${OPT1S[@]}" ; do
						case "$REPLY" in
						  1|2 ) break;;
						  3 ) errorQuit;;
						  *) errorContinue;;
						esac
					done

					if [[ $OPT1 == "Yes" ]]; then
					  redMessage "Removing old CloudNet ZIP file..."
					  rm -rf $CNDLDIR/CloudNet.zip >/dev/null 2>&1 #

						if [ ! -f "$CNDLDIR/CloudNet.zip" ] ; then
						  greenMessage "Old CloudNet ZIP file removed!"
						else
						  redMessage "Couldn't remove CloudNet ZIP file. Please remove it manually."
						fi
					elif [[ $OPT1 == "No" ]] ; then
					  errorExit
					fi
				fi
			fi

		  echo ""
		  cyanMessage "Downloading CloudNet ZIP file... This may take up to 2 minutes..."

		  rm -rf "$CNDLDIR"/CloudNet.zip >/dev/null 2>&1 #
		  wget -q -O "$CNDLDIR"/CloudNet.zip http://dytanic.de/cloudnetproject/CloudNet.zip >/dev/null 2>&1 #

		  echo ""

			if [ -f "$CNDLDIR"/CloudNet.zip ] ; then
			  greenMessage "CloudNet ZIP file downloaded successfully!"
			else
			  redMessage "CloudNet ZIP file download failed!"
			  errorExit
			fi

		  yellowMessage "Should the installer extract the files?"

			OPT5S=("Yes" "No" "Quit")
			select OPT5 in "${OPT5S[@]}" ; do
				case "$REPLY" in
				  1|2|3 ) break;;
				  4 ) errorQuit;;
				  *) errorContinue;;
				esac
			done

		  cd "$CNDLDIR"

			if [[ $OPT5 == "Yes" ]]; then

			  cyanMessage "Unpacking CloudNet ZIP file..."

			  cd /tmp >/dev/null 2>&1 #
			  rm -rf /tmp/CloudNet.zip >/dev/null 2>&1 #
			  wget -q -O /tmp/CloudNet.zip http://dytanic.de/cloudnetproject/CloudNet.zip >/dev/null 2>&1 #
			  rm -rf /tmp/.cloudnetinstaller >/dev/null 2>&1 #
			  mkdir -p /tmp/.cloudnetinstaller >/dev/null 2>&1 #
			  unzip CloudNet.zip -d ./.cloudnetinstaller >/dev/null 2>&1 #
			  rm -rf /tmp/CloudNet-Proxy >/dev/null 2>&1 #
			  rm -rf /tmp/CloudNet-Server >/dev/null 2>&1 #
			  cp -R /tmp/.cloudnetinstaller/CloudNet-Proxy . >/dev/null 2>&1 #
			  cp -R /tmp/.cloudnetinstaller/CloudNet-Server . >/dev/null 2>&1 #
			  cp -R /tmp/.cloudnetinstaller/* "$CNDLDIR" >/dev/null 2>&1 #
			  rm -rf /tmp/CloudNet-Proxy >/dev/null 2>&1 #
			  rm -rf /tmp/CloudNet-Server >/dev/null 2>&1 #
			  rm -rf /tmp/.cloudnetinstaller >/dev/null 2>&1 #
			  rm -rf /tmp/CloudNet.zip >/dev/null 2>&1 #

				if [ -d "$CNDLDIR"/dev ] || [ -d "$CNDLDIR"/tools ] || [ -d "$CNDLDIR"/signlayouts ]; then

				  greenMessage "Successfully unpacked directories /dev, /tools and /signlayouts"

				else

				  redMessage "Couldn't unpack ZIP file! Please unpack it manually."

				fi

			elif [[ $OPT5 == "No" ]]; then

			  return 1

			fi

		fi

	elif [[ $OPT1 == "Remove" ]]; then

	  echo ""
	  yellowMessage "What should be removed?"

		OPT5S=("Proxy" "Server" "Both" "Quit")
		select OPT5 in "${OPT5S[@]}" ; do
			case "$REPLY" in
			  1|2|3 ) break;;
			  4 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT5 == "Proxy" ]]; then

		  remove_proxy_survey

		elif [[ $OPT5 == "Server" ]]; then

		  remove_server_survey

		elif [[ $OPT5 == "Both" ]]; then

		  remove_proxy_survey
		  remove_server_survey

		fi

	fi

  echo ""
  magentaMessage "Thank you for using this installer. For support, feature requests & bug reports, feel free to contact me on\nTwitter: https://twitter.com/MiniMinerLPs \nTeamSpeak: MiniMinerLPs.de\nSupport Discord: https://discord.gg/awQn9aq \nor use my request form: https://goo.gl/1jAp1a"
  echo ""
}

clear
cd /
initial
